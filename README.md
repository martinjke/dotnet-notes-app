# NoteApp

App was built using:

- Angular 8.1.2
- .NET Core 3.1

## Development

```bash

# install npm dependencies
cd client
npm install

# run front-end app
# and open browser at http://localhost:9000
npm start

# run back-end app
# and open browser at http://localhost:5000/swagger
cd server/NoteApp.WebApi
dotnet run
```
