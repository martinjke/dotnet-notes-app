# NotesApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:9000/`. The app will automatically reload if you change any of the source files.

## Mock server 1

Run `npm run start:mock` for a json server.
Enpoint will be available on `http://localhost:3000`

## Mock server 2

Run `dotnet run` at server/NoteApp.WebApi folder.
Enpoint will be available on `http://localhost:5000/api`
Swagger: `http://localhost:5000/swagger`

## Run test

Run `npm run test` for tests
