using System.ComponentModel.DataAnnotations;

namespace NoteApp.WebApi.Models.BindingModels
{
    /// <summary>
    /// 
    /// </summary>
    public class NoteBindingModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [StringLength(600)]
        public string Description { get; set; }
    }
}