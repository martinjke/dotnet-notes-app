using System.ComponentModel.DataAnnotations;

namespace NoteApp.WebApi.Models.BindingModels
{
    /// <summary>
    /// 
    /// </summary>
    public class NoteViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }
}