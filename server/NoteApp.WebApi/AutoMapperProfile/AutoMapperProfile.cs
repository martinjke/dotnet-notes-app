﻿using AutoMapper;
using NoteApp.Services.DTO;
using NoteApp.WebApi.Models.BindingModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoteApp.WebApi.AutoMapperProfile
{
    /// <summary>
    /// Defines mapping rules
    /// </summary>
    public class AutoMapperWebApiProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public AutoMapperWebApiProfile()
        {
            CreateMap<NoteBindingModel, NoteDTO>().ReverseMap();
            CreateMap<NoteViewModel, NoteDTO>().ReverseMap();
        }
    }
}
