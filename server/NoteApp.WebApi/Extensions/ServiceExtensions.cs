﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using NoteApp.Core;
using NoteApp.Services;
using NoteApp.Services.Interfaces;
using NoteApp.Services.Services;
using NoteApp.WebApi.Attributes;
using NoteApp.WebApi.AutoMapperProfile;
using System;
using System.IO;
using System.Reflection;

namespace NoteApp.WebApi.Extensions
{
    /// <summary>
    /// Configure services 
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        /// Configures cors
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                    );
            });
        }
        /// <summary>
        /// Configures in memory database
        /// </summary>
        /// <param name="services"></param>

        public static void ConfigureInMemoryDatabase(this IServiceCollection services)
        {
            services.AddDbContext<NoteContext>(opt =>
            {
                opt.UseInMemoryDatabase("Notes");
            });
        }
        /// <summary>
        /// Applies Model state validation filter
        /// </summary>
        /// <param name="services"></param>

        public static void ConfigureValidateModelAttribute(this IServiceCollection services)
        {
            services.AddScoped<ValidateModelAttribute>();
        }
        /// <summary>
        /// Defines services DI
        /// </summary>
        /// <param name="services"></param>

        public static void ConfigureAppServices(this IServiceCollection services)
        {
            services.AddScoped<INoteService, NoteService>();
        }

        /// <summary>
        /// Configures swagger
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My Notes API", Version = "v1" });

                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }
        /// <summary>
        /// Configures automapper profiles
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperWebApiProfile());
                cfg.AddProfile(new AutomapperServicesProfile());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureAll(this IServiceCollection services)
        {
            services.ConfigureCors();
            services.ConfigureInMemoryDatabase();
            services.ConfigureValidateModelAttribute();
            services.ConfigureAppServices();
            services.ConfigureSwagger();
            services.ConfigureAutoMapper();
        }
    }
}
