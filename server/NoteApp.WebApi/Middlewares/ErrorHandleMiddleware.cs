﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using NoteApp.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NoteApp.WebApi.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger _logger;

        /// <summary>
        ///
        /// </summary>
        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>()
                ?? throw new ArgumentNullException(nameof(loggerFactory)); ;
            this.next = next;
        }

        /// <summary>
        ///
        /// </summary>
        public async Task Invoke(HttpContext context /* other scoped dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.GetType()}");
                if (ex is NotFoundException)
                {
                    _logger.LogError(ex, ex.Message);
                }
                else
                {
                    _logger.LogCritical(ex, ex.Message);
                }
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var message = "";
            String reasonPhrase = String.Empty;
            if (exception is NotFoundException)
            {
                reasonPhrase = "NotFound";
                code = HttpStatusCode.NotFound;
                message = exception.Message;
            }
           
            else
            {
                reasonPhrase = "Unexpected";
                code = HttpStatusCode.InternalServerError;
                message = "Unexpected exception";
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            context.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = reasonPhrase;
            return context.Response.WriteAsync(new ErrorDetails()
            {
                Message = message,
                StatusCode = (int)code
            }.ToString());
        }
    }
}
