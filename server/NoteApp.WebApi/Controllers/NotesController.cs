﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NoteApp.Services.DTO;
using NoteApp.Services.Interfaces;
using NoteApp.WebApi.Middlewares;
using NoteApp.WebApi.Models.BindingModels;

namespace NoteApp.WebApi.Controllers
{
    /// <summary>
    /// Note controller
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INoteService _notesService;
        private readonly ILogger<NotesController> _logger;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notesService"></param>
        /// <param name="mapper"></param>
        /// <param name="logger"></param>
        public NotesController(
            INoteService notesService,
            IMapper mapper,
            ILogger<NotesController> logger)
        {
            _notesService = notesService;
            _logger = logger;
            _mapper = mapper;
        }
        /// <summary>
        /// Get All Notes
        /// </summary>
        /// <remarks>
        /// Sample Request
        /// 
        ///     GET /api/Notes
        ///     
        /// </remarks>
        /// <returns>Returns list of notes</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<NoteViewModel>), 200)]
        public async Task<IActionResult> GetNotesAsync()
        {
            _logger.LogInformation($"GET {nameof(GetNotesAsync)}");
            var notes = await _notesService.GetAll();

            var res = _mapper.Map<IList<NoteViewModel>>(notes);
            _logger.LogInformation($"200 OK : GET {nameof(GetNotesAsync)}");
            return Ok(res);
        }

        /// <summary>
        /// Get Note by Id
        /// </summary>
        /// <remarks>
        /// Sample Request
        /// 
        ///     GET /api/Notes/1
        ///     
        /// </remarks>
        /// <param name="id">note id</param>
        /// <returns>Returns note</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(NoteViewModel), 200)]
        [ProducesResponseType(typeof(ErrorDetails), 404)]
        public async Task<IActionResult> GetNoteById([FromRoute]int id)
        {
            _logger.LogInformation($"GET {nameof(GetNoteById)} with {id}");
            var note = await _notesService.GetById(id);

            var res = _mapper.Map<NoteViewModel>(note);
            _logger.LogInformation($"200 OK : GET {nameof(GetNoteById)}");
            return Ok(res);
        }

        /// <summary>
        /// Create new note
        /// </summary>
        /// <remarks>
        /// Sample Request
        /// 
        ///     POST /api/Notes
        ///     {
        ///         "name": "Note 1",
        ///         "description": "Lorem ispum"
        ///     }
        /// 
        /// </remarks>
        /// <param name="model">new note</param>
        /// <returns>new note</returns>
        [HttpPost]
        [ProducesResponseType(typeof(NoteViewModel), 201)]
        public async Task<IActionResult> CreateNote([FromBody] NoteBindingModel model)
        {
            _logger.LogInformation($"POST {nameof(CreateNote)}");

            var modelDto = _mapper.Map<NoteDTO>(model);
            var note = await _notesService.Create(modelDto);

            var res = _mapper.Map<NoteViewModel>(note);
            _logger.LogInformation($"201 OK : POST {nameof(CreateNote)}");
            return CreatedAtAction(nameof(GetNoteById), new { id = res.Id }, res);
        }

        /// <summary>
        /// Update note with id
        /// </summary>
        /// <remarks>
        /// Sample Request
        /// 
        ///     PUT /api/Notes/1
        ///     {
        ///         "name": "Note 1",
        ///         "description": "Lorem ispum"
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">note id to update</param>
        /// <param name="model">note model</param>
        /// <returns>updated note</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(NoteViewModel), 200)]
        [ProducesResponseType(typeof(ErrorDetails), 404)]
        public async Task<IActionResult> EditNote([FromRoute] int id, [FromBody] NoteBindingModel model)
        {
            _logger.LogInformation($"PUT {nameof(EditNote)} with {id}");
            var modelDto = _mapper.Map<NoteDTO>(model);
            var note = await _notesService.Edit(id, modelDto);

            var res = _mapper.Map<NoteViewModel>(note);
            _logger.LogInformation($"200 OK : PUT {nameof(EditNote)}");
            return Ok(res);
        }

        /// <summary>
        /// Remove note with id
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///     DELETE /api/Notes/1
        ///     
        /// </remarks>
        /// <param name="id">note id</param>
        /// <returns>no content</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorDetails), 404)]
        public async Task<IActionResult> RemoveNote([FromRoute] int id)
        {
            _logger.LogInformation($"DELETE {nameof(RemoveNote)} with {id}");
            await _notesService.Remove(id);

            _logger.LogInformation($"200 OK : DELETE {nameof(RemoveNote)} with {id}");
            return NoContent();
        }
    }
}