using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NoteApp.Core;
using NoteApp.Core.Entities;
using NoteApp.Infrastructure.Data;
using NoteApp.Services;
using NoteApp.Services.DTO;
using NoteApp.Services.Infrastructure;
using NoteApp.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NoteApp.UnitTests
{
    public class NoteServiceTests
    {
        [Fact]
        public async Task GetAllNotes_Success()
        {
            // Arrange
            var service = CreateService();

            // Act
            var notes = await service.GetAll();
            var count = notes.Count;

            // Assert
            Assert.Equal(30, count);
        }
        [Fact]
        public async Task GetNoteById_Success()
        {
            // Arrange
            var service = CreateService();
            var id = 1;
            var expectedNote = new NoteDTO
            {
                Id = 1,
                Name = "Note 1",
                Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
            };

            // Act
            var note = await service.GetById(id);



            // Assert
            var res = note.Equals(expectedNote);
            Assert.True(res);
        }
        [Fact]
        public async Task GetNoteByNonExistingId_NotFoundException()
        {
            // Arrange
            var service = CreateService();
            var id = 31;
            // Act
            var ex = await Record.ExceptionAsync(async () => await service.GetById(id));

            // Assert
            var res = ex is NotFoundException;
            Assert.True(res);
            Assert.Equal($"Entity {nameof(Note)} with key {id} not found", ex.Message);
        }
        [Fact]
        public async Task CreateNewNote_Success()
        {
            // Arrange
            var service = CreateService();
            var noteDto = new NoteDTO
            {
                Name = "Note",
                Description = "Description"
            };

            // Act
            var createdNote = await service.Create(noteDto);


            // Assert
            var res = string.Equals(noteDto.Name, createdNote.Name) &&
                string.Equals(noteDto.Description, createdNote.Description);
            Assert.True(res);
        }
        [Fact]
        public async Task EditNote_Success()
        {
            // Arrange
            var service = CreateService();
            int id = 1;
            var noteDto = new NoteDTO
            {
                Name = "Note",
                Description = "Description"
            };

            // Act
            var updatedNote = await service.Edit(id, noteDto);


            // Assert
            var res = updatedNote.Id == id &&
                string.Equals(noteDto.Name, updatedNote.Name) &&
                string.Equals(noteDto.Description, updatedNote.Description);
            Assert.True(res);
        }
        [Fact]
        public async Task EditNoteWithNonExistingId_NotFoundException()
        {
            // Arrange
            var service = CreateService();
            int id = 31;
            var noteDto = new NoteDTO
            {
                Name = "Note",
                Description = "Description"
            };

            // Act
            var ex = await Record.ExceptionAsync(async () => await service.Edit(id, noteDto));

            // Assert
            var res = ex is NotFoundException;
            Assert.True(res);
            Assert.Equal($"Entity {nameof(Note)} with key {id} not found", ex.Message);
        }
        [Fact]
        public async Task RemoveNote_Success()
        {
            // Arrange
            var service = CreateService();
            int id = 1;

            // act
            await service.Remove(id);
            var allNotes = await service.GetAll();
            var count = allNotes.Count;

            // Assert
            Assert.Equal(29, count);
        }
        [Fact]
        public async Task RemoveNoteWithNonExistingId_NotFoundException()
        {
            // Arrange
            var service = CreateService();
            int id = 31;

            // act
            var ex = await Record.ExceptionAsync(async () => await service.Remove(id));

            // Assert
            var res = ex is NotFoundException;
            Assert.True(res);
            Assert.Equal($"Entity {nameof(Note)} with key {id} not found", ex.Message);
        }

        #region mock
        private NoteContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<NoteContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new NoteContext(options);
            var notes = DataSeeder.GetFakeData();
            context.Notes.AddRange(notes);
            context.SaveChanges();
            return context;
        }

        private NoteService CreateService()
        {
            var context = CreateDbContext();
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutomapperServicesProfile());
            });
            var mapper = mockMapper.CreateMapper();
            var service = new NoteService(context, mapper);
            return service;
        }
        #endregion
    }
}
