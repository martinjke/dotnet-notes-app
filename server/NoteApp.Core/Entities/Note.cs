﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace NoteApp.Core.Entities
{
    public class Note : IEquatable<Note>
    {
        public int Id { get; set; }

        [Required]
        [StringLength (60, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [StringLength (600)]
        public string Description { get; set; }

        public bool Equals(Note other)
        {
            if (other == null) return false;
            return Id == other.Id &&
                string.Equals(Name, other.Name) &&
                string.Equals(Description, other.Description);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals(obj as Note);
        }
    }
}