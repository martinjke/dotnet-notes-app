﻿using Microsoft.EntityFrameworkCore;
using NoteApp.Core.Entities;

namespace NoteApp.Core
{
    public class NoteContext : DbContext
    {
        public NoteContext(DbContextOptions<NoteContext> options) : base(options)
        { }
        public NoteContext() { }
        public virtual DbSet<Note> Notes { get; set; }
    }
}
