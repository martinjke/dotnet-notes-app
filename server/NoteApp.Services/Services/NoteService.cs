﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NoteApp.Core;
using NoteApp.Core.Entities;
using NoteApp.Services.DTO;
using NoteApp.Services.Infrastructure;
using NoteApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NoteApp.Services.Services
{

    public class NoteService : INoteService
    {
        private readonly NoteContext _context;
        private readonly IMapper _mapper;
        public NoteService(
            NoteContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<NoteDTO> Create(NoteDTO dto)
        {
            var note = _mapper.Map<Note>(dto);

            await _context.Notes.AddAsync(note);
            await _context.SaveChangesAsync();

            return _mapper.Map<NoteDTO>(note);
        }

        public async Task<NoteDTO> Edit(int id, NoteDTO dto)
        {
            var note = await GetNoteById(id);

            note.Name = dto.Name;
            note.Description = dto.Description;

            _context.Notes.Update(note);
            await _context.SaveChangesAsync();

            return _mapper.Map<NoteDTO>(note);
        }

        public async Task<IList<NoteDTO>> GetAll()
        {
            var notes = await _context.Notes.ToListAsync();
            var res = _mapper.Map<IList<NoteDTO>>(notes);
            return res;
        }

        public async Task<NoteDTO> GetById(int id)
        {
            var note = await GetNoteById(id);
            var res = _mapper.Map<NoteDTO>(note);
            return res;
        }

        public async Task Remove(int id)
        {
            var note = await GetNoteById(id);
            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();
        }


        private async Task<Note> GetNoteById(int id)
        {
            var note = await _context.Notes.FindAsync(id);
            if (note == null)
            {
                throw new NotFoundException(nameof(Note), id);
            }
            return note;
        }
    }
}
