﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace NoteApp.Services.DTO
{
    public class NoteDTO : IEquatable<NoteDTO>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public bool Equals(NoteDTO other)
        {
            if (other == null) return false;
            return Id == other.Id &&
                string.Equals(Name, other.Name) &&
                string.Equals(Description, other.Description);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals(obj as NoteDTO);
        }
    }
}
