﻿using AutoMapper;
using NoteApp.Core.Entities;
using NoteApp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoteApp.Services
{
    public class AutomapperServicesProfile : Profile
    {
        public AutomapperServicesProfile()
        {
            CreateMap<NoteDTO, Note>().ReverseMap();
        }
    }
}
