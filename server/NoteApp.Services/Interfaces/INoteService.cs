﻿using NoteApp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NoteApp.Services.Interfaces
{
    public interface INoteService
    {
        /// <summary>
        /// Gets all notes
        /// </summary>
        /// <returns>list of all notes</returns>
        Task<IList<NoteDTO>> GetAll();
        /// <summary>
        /// Gets note by id
        /// </summary>
        /// <param name="id">note id to find</param>
        /// <returns>note</returns>
        Task<NoteDTO> GetById(int id);
        /// <summary>
        /// Creates new note
        /// </summary>
        /// <param name="model">note model</param>
        /// <returns>created note</returns>
        Task<NoteDTO> Create(NoteDTO model);
        /// <summary>
        /// Edits note
        /// </summary>
        /// <param name="id">note id to edit</param>
        /// <param name="model">new note model</param>
        /// <returns>edited note</returns>
        Task<NoteDTO> Edit(int id, NoteDTO model);
        /// <summary>
        /// Removes note
        /// </summary>
        /// <param name="id">note id to remove</param>
        /// <returns></returns>
        Task Remove(int id);
    }
}
