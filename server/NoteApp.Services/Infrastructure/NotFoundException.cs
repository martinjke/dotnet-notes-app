﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoteApp.Services.Infrastructure
{
    public class NotFoundException : Exception
    {
        public NotFoundException() { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string name, object key) : base($"Entity {name} with key {key} not found") { }
        
    }
}
