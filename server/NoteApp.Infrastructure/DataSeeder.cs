﻿using Microsoft.EntityFrameworkCore.Internal;
using NoteApp.Core;
using NoteApp.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoteApp.Infrastructure.Data
{
    public static class DataSeeder
    {
        public static void Seed(NoteContext context)
        {
            context.Database.EnsureCreated();

            if (context.Notes.Any())
            {
                return;
            }

            var notes = GetFakeData();
            context.AddRange(notes);
            context.SaveChanges();
        }

        public static List<Note> GetFakeData()
        {
            var notes = new List<Note>();

            for (int i = 1; i <= 30; ++i)
            {
                notes.Add(new Note
                {
                    Id = i,
                    Name = $"Note {i}",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
                });
            }
            return notes;
        }
    }
}
